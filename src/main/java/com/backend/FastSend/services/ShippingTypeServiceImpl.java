package com.backend.FastSend.services;

import com.backend.FastSend.models.dto.ShippingTypeDto;
import com.backend.FastSend.models.entities.ShippingType;
import com.backend.FastSend.repositories.ShippingTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ShippingTypeServiceImpl implements ShippingTypeService {
    @Autowired
    private ShippingTypeRepository shippingTypeRepository;

    @Override
    @Transactional(readOnly = true)
    public List<ShippingTypeDto> findAll() {
        return shippingTypeRepository.findAll()
                .stream()
                .map(this::convertEntityToDto)
                .collect(Collectors.toList());
    }

    private ShippingTypeDto convertEntityToDto(ShippingType shippingType) {
        ShippingTypeDto shippingTypeDto = new ShippingTypeDto();
        shippingTypeDto.setId(shippingType.getId());
        shippingTypeDto.setName(shippingType.getName());
        return shippingTypeDto;
    }
}

