package com.backend.FastSend.services;

import com.backend.FastSend.models.base.NotFoundException;
import com.backend.FastSend.models.dto.ShipmentDto;
import com.backend.FastSend.models.dto.ShipmentTypeDto;
import com.backend.FastSend.models.dto.ShippingTypeDto;
import com.backend.FastSend.models.entities.*;
import com.backend.FastSend.models.request.RegisterShipmentRequest;
import com.backend.FastSend.models.request.ShipmentDetailRequest;
import com.backend.FastSend.repositories.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ShipmentServiceImpl implements ShipmentService
{
    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private ShipmentRepository shipmentRepository;

    @Autowired
    private ShipmentDetailRepository shipmentDetailRepository;

    @Autowired
    private ShippingTypeRepository shippingTypeRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private ShipmentTypeRepository shipmentTypeRepository;

    @Autowired
    private ShipmentRateRepository shipmentRateRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    @Transactional(readOnly = true)
    public List<ShipmentDto> findAll() {
        return shipmentRepository.findAll()
                .stream()
                .map(this::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<ShipmentTypeDto> findAllShipmentType() {
        return shipmentTypeRepository.findAll()
                .stream()
                .map(this::convertEntityToDtoShipmentType)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ShipmentDto> findById(Long id) {
        Optional<Shipment> companyOptional = shipmentRepository.findById(id);
        return companyOptional.map(this::convertEntityToDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ShipmentDto> findByIdEmployee(Long idEmployee) {
        return shipmentRepository.findAll()
                .stream()
                .filter(jobOffer -> jobOffer.getEmployeeReceptionist().getId().equals(idEmployee))
                .map(this::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public Optional<ShipmentDto> save(RegisterShipmentRequest request) {
        Shipment shipment = convertDtoToEntityShipment(request);
        Shipment saveShipment = shipmentRepository.save(shipment);

        for (ShipmentDetailRequest detailRequest : request.getShipmentDetails()) {
            ShipmentDetail shipmentDetail = convertDtoToEntityShipmentDetail(detailRequest, saveShipment.getId());
            shipmentDetailRepository.save(shipmentDetail);
        }

        Optional<Shipment> companyOptional = shipmentRepository.findById(saveShipment.getId());
        return companyOptional.map(this::convertEntityToDto);
    }

    @Override
    @Transactional
    public ShipmentDto saveDelivery(Long id, Long idEmployee) {
        Shipment shipment = shipmentRepository.findById(id).orElseThrow();

        if (!shipment.getPaid()) {
            // Si no está pagado, retornar con un mensaje
            throw new RuntimeException("La encomienda no puede ser entregada sin el pago correspondiente");
        }

        shipment.setDate_time_delivery(LocalDateTime.now());
        shipment.setState(2L);

        Employee employeeDelivery = employeeRepository.findById(idEmployee).orElseThrow();
        shipment.setEmployeeDelivery(employeeDelivery);

        Shipment updateShipment = shipmentRepository.save(shipment);

        return convertEntityToDto(updateShipment);
    }

    @Override
    @Transactional
    public ShipmentDto savePaid(Long id) {
        Shipment shipment = shipmentRepository.findById(id).orElseThrow();
        shipment.setPaid(true);
        Shipment updateShipment = shipmentRepository.save(shipment);
        return convertEntityToDto(updateShipment);
    }

    @Override
    public Double calculateShipmentCost(Integer quantity, Long idBranchDestination, Long idBranchOrigin, Long idShipmentType, Long idShippingType) {
        ShipmentRate shipmentRate = shipmentRateRepository.findByCriteria(idBranchDestination, idBranchOrigin, idShipmentType, idShippingType);

        if (shipmentRate != null) {
            return quantity * shipmentRate.getPrice();
        } else {
            throw new NotFoundException("No se encontró una tarifa de envío para los criterios dados.");
        }
    }
    private ShipmentTypeDto convertEntityToDtoShipmentType(ShipmentType shipment) {
        ShipmentTypeDto shipmentDto = new ShipmentTypeDto();
        shipmentDto.setId(shipment.getId());
        shipmentDto.setName(shipment.getName());
        return shipmentDto;
    }

    private ShipmentDto convertEntityToDto(Shipment shipment) {
        ShipmentDto shipmentDto = new ShipmentDto();
        shipmentDto.setId(shipment.getId());
        shipmentDto.setDate_time_reception(shipment.getDate_time_reception());
        shipmentDto.setDate_time_delivery(shipment.getDate_time_delivery());
        shipmentDto.setObservation(shipment.getObservation());
        shipmentDto.setTotal_shipping_rate(shipment.getTotal_shipping_rate());
        shipmentDto.setPaid(shipment.getPaid());
        shipmentDto.setState(shipment.getState());
        shipmentDto.setVisible(shipment.getVisible());
        shipmentDto.setRemoved(shipment.getRemoved());
        shipmentDto.setShippingType(shipment.getShippingTypeDto());
        shipmentDto.setCustomerSender(shipment.getCustomerSenderDto());
        shipmentDto.setCustomerAddressee(shipment.getCustomerAdresseeDto());
        shipmentDto.setEmployeeReceptionist(shipment.getEmployeeReceptionistDto());
        shipmentDto.setEmployeeDelivery(shipment.getEmployeeDeliveryDto());
        shipmentDto.setBranchOrigin(shipment.getBranchOriginDto());
        shipmentDto.setBranchDestination(shipment.getBranchDestinationDto());
        shipmentDto.setShipmentDetails(shipment.getShipmentDetailDto());

        return shipmentDto;
    }

    private Shipment convertDtoToEntityShipment(RegisterShipmentRequest request) {
        Shipment shipment = new Shipment();
        shipment.setDate_time_reception(LocalDateTime.now());
        shipment.setDate_time_delivery(null);
        shipment.setObservation(request.getObservation());
        shipment.setTotal_shipping_rate(request.getTotal_shipping_rate());
        shipment.setPaid(request.getPaid());
        shipment.setState(1L);
        shipment.setVisible(true);
        shipment.setRemoved(false);
        ShippingType shippingType = shippingTypeRepository.findById(request.getIdShippingType()).orElseThrow();
        shipment.setShippingType(shippingType);
        Customer customerSender = customerRepository.findById(request.getIdCustomerSender()).orElseThrow();
        shipment.setCustomerSender(customerSender);
        Customer customerAddressee = customerRepository.findById(request.getIdCustomerAddressee()).orElseThrow();
        shipment.setCustomerAddressee(customerAddressee);
        Employee employeeReceptionist = employeeRepository.findById(request.getIdEmployee()).orElseThrow();
        shipment.setEmployeeReceptionist(employeeReceptionist);
        shipment.setEmployeeDelivery(null);
        Branch branchOrigin = branchRepository.findById(request.getIdBranchOrigin()).orElseThrow();
        shipment.setBranchOrigin(branchOrigin);
        Branch branchDestination = branchRepository.findById(request.getIdBranchDestination()).orElseThrow();
        shipment.setBranchDestination(branchDestination);

        return shipment;
    }


    private ShipmentDetail                                                                                                                                                     convertDtoToEntityShipmentDetail(ShipmentDetailRequest request, Long idShipment) {
        ShipmentDetail shipmentDetail = new ShipmentDetail();
        shipmentDetail.setContent(request.getContent());
        shipmentDetail.setWeight(request.getWeight());
        shipmentDetail.setAdditional_volume_surcharge(request.getAdditional_volume_surcharge());
        shipmentDetail.setShipping_cost(request.getShipping_cost());
        shipmentDetail.setRemoved(false);
        ShipmentType shipmentType = shipmentTypeRepository.findById(request.getIdShipmentType()).orElseThrow();
        shipmentDetail.setShipmentType(shipmentType);
        Shipment shipment = shipmentRepository.findById(idShipment).orElseThrow();
        shipmentDetail.setShipment(shipment);

        return shipmentDetail;
    }
}
