package com.backend.FastSend.services;

import com.backend.FastSend.models.dto.ShiftDto;
import com.backend.FastSend.models.dto.ShipmenRateDto;
import com.backend.FastSend.models.entities.Shift;
import com.backend.FastSend.repositories.ShiftRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ShiftServiceImpl implements ShiftService{
    @Autowired
    private ShiftRepository shiftRepository;

    @Override
    @Transactional(readOnly = true)
    public List<ShiftDto> findAll() {
        return shiftRepository.findAll()
                .stream()
                .map(this::convertEntityToDto)
                .collect(Collectors.toList());
    }

    private ShiftDto convertEntityToDto(Shift shift) {
        ShiftDto shiftDto = new ShiftDto();
        shiftDto.setId(shift.getId());
        shiftDto.setName(shift.getName());
        shiftDto.setStart_time(shift.getStart_time());
        shiftDto.setEnd_time(shift.getEnd_time());
        return shiftDto;
    }
}
