package com.backend.FastSend.services;

import com.backend.FastSend.models.dto.EmployeeDto;
import com.backend.FastSend.models.dto.UserDto;
import com.backend.FastSend.models.dto.TypeUserDto;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<UserDto> findAll();

    Optional<TypeUserDto> findById(Long id);

    UserDto save(UserDto user);

    EmployeeDto authenticate(String email, String password);
}
