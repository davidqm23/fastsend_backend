package com.backend.FastSend.services;

import com.backend.FastSend.models.dto.CustomerDto;
import com.backend.FastSend.models.request.RegisterCustomerRequest;
import com.backend.FastSend.models.request.RegisterEmployeeRequest;

import java.util.List;

public interface CustomerService {

    List<CustomerDto> findAll();

    List<CustomerDto> searchByName(String searchTerm, int maxResults);

    CustomerDto save(RegisterCustomerRequest customerRequest);

}
