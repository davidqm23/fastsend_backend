package com.backend.FastSend.services;

import com.backend.FastSend.models.dto.ShippingTypeDto;
import com.backend.FastSend.models.entities.ShippingType;

import java.util.List;

public interface ShippingTypeService {
    List<ShippingTypeDto> findAll();
}
