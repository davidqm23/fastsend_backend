package com.backend.FastSend.services;

import com.backend.FastSend.models.dto.EmployeeDto;
import com.backend.FastSend.models.entities.*;
import com.backend.FastSend.models.request.RegisterEmployeeRequest;
import com.backend.FastSend.repositories.EmployeeRepository;
import com.backend.FastSend.repositories.ShiftRepository;
import com.backend.FastSend.repositories.TypeUserRepository;
import com.backend.FastSend.repositories.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl implements EmployeeService
{
    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TypeUserRepository typeUserRepository;

    @Autowired
    private ShiftRepository shiftRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    @Transactional(readOnly = true)
    public List<EmployeeDto> findAll() {
        return employeeRepository.findAll()
                .stream()
                .map(this::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<EmployeeDto> findById(Long id) {
        Optional<Employee> companyOptional = employeeRepository.findById(id);
        return companyOptional.map(this::convertEntityToDto);
    }

    @Override
    @Transactional
    public EmployeeDto saveAdministrator(RegisterEmployeeRequest request) {
        User user = convertDtoToEntityUserAdministrator(request);
        userRepository.save(user); // Guardar el User primero

        Employee employee = convertDtoToEntityEmployee(request);
        employee.setUser(user); // Asociar el User con el Company
        Employee savedEmployee = employeeRepository.save(employee); // Guardar el Company

        return convertEntityToDto(savedEmployee);
    }

    @Override
    @Transactional
    public EmployeeDto saveReceptionist(RegisterEmployeeRequest request) {
        User user = convertDtoToEntityUserReceptionist(request);
        User userSave = userRepository.save(user); // Guardar el User primero

        Employee employee = convertDtoToEntityEmployee(request);
        employee.setUser(user); // Asociar el User con el Company
        Employee savedEmployee = employeeRepository.save(employee); // Guardar el Company

        return convertEntityToDtoB(savedEmployee, userSave);
    }

    private EmployeeDto convertEntityToDtoB(Employee employee, User user) {
        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setId(employee.getId());
        employeeDto.setName(employee.getName());
        employeeDto.setLast_name(employee.getLast_name());
        employeeDto.setEmail(user.getEmail());
        employeeDto.setBirthdate(employee.getBirthdate());
        employeeDto.setGender(employee.getGender());
        employeeDto.setCod_area(employee.getCod_area());
        employeeDto.setNumber_phone(employee.getNumber_phone());
        employeeDto.setIdentityCard(employee.getIdentity_card());
        employeeDto.setExtensionIdentityCard(employee.getExtension_identity_card());
        employeeDto.setAddress(employee.getAddress());
        employeeDto.setPhoto(employee.getPhoto());
        employeeDto.setShift(employee.getShiftDto());

        return employeeDto;
    }

    private EmployeeDto convertEntityToDto(Employee employee) {
        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setId(employee.getId());
        employeeDto.setName(employee.getName());
        employeeDto.setLast_name(employee.getLast_name());
        employeeDto.setBirthdate(employee.getBirthdate());
        employeeDto.setGender(employee.getGender());
        employeeDto.setCod_area(employee.getCod_area());
        employeeDto.setNumber_phone(employee.getNumber_phone());
        employeeDto.setIdentityCard(employee.getIdentity_card());
        employeeDto.setExtensionIdentityCard(employee.getExtension_identity_card());
        employeeDto.setAddress(employee.getAddress());
        employeeDto.setPhoto(employee.getPhoto());
        employeeDto.setShift(employee.getShiftDto());
        User user = userRepository.findById(employee.getUser().getId()).orElseThrow();
        employeeDto.setEmail(user.getEmail());
        employeeDto.setTypeUser(user.getTypeUserDto());

        return employeeDto;
    }

    private User convertDtoToEntityUserAdministrator(RegisterEmployeeRequest request) {
        User user = new User();
        user.setEmail(request.getUser());
        user.setRemoved(false);
        TypeUser typeUser = typeUserRepository.findById(1L).orElseThrow();
        user.setTypeUser(typeUser);
        return user;
    }

    private User convertDtoToEntityUserReceptionist(RegisterEmployeeRequest request) {
        User user = new User();
        user.setEmail(request.getUser());
        user.setPassword(request.getPassword());
        user.setRemoved(false);
        TypeUser typeUser = typeUserRepository.findById(2L).orElseThrow();
        user.setTypeUser(typeUser);
        return user;
    }

    private Employee convertDtoToEntityEmployee(RegisterEmployeeRequest request) {
        Employee employee = new Employee();
        employee.setName(request.getName());
        employee.setLast_name(request.getLastName());
        employee.setBirthdate(request.getBirthdate());
        employee.setGender(request.getGender());
        employee.setCod_area(request.getCod_area());
        employee.setNumber_phone(request.getNumberPhone());
        employee.setIdentity_card(request.getIdentityCard());
        employee.setExtension_identity_card(request.getExtensionIdentityCard());
        employee.setAddress(request.getAddress());
        employee.setPhoto(request.getPhoto());
        employee.setVisible(true);
        employee.setRemoved(false);

        if (request.getId_shift() != null) {
            Shift shift = shiftRepository.findById(request.getId_shift()).orElse(null);
            employee.setShift(shift);
        }
        return employee;
    }
}
