package com.backend.FastSend.services;

import com.backend.FastSend.models.dto.BranchDto;
import com.backend.FastSend.models.dto.ShipmentDto;

import java.util.List;

public interface BranchService {
    List<BranchDto> findAll();
}
