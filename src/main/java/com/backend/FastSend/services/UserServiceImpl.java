package com.backend.FastSend.services;

import com.backend.FastSend.models.dto.EmployeeDto;
import com.backend.FastSend.models.dto.TypeUserDto;
import com.backend.FastSend.models.dto.UserDto;
import com.backend.FastSend.models.entities.Employee;
import com.backend.FastSend.models.entities.User;
import com.backend.FastSend.repositories.EmployeeRepository;
import com.backend.FastSend.repositories.UserRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository repository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private ModelMapper modelMapper;
    @Override
    @Transactional(readOnly = true)
    public List<UserDto> findAll() {
        return repository.findAll()
                .stream()
                .map(this::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<TypeUserDto> findById(Long id) {
        Optional<User> userOptional = repository.findById(id);
        return userOptional.map(this::convertEntityToDtoB);
    }

    @Override
    @Transactional
    public UserDto save(UserDto userDto) {
        User user = convertDtoToEntity(userDto);
        User savedUser = repository.save(user);
        return convertEntityToDto(savedUser);
    }

    @Override
    public EmployeeDto authenticate(String email, String password) {
        User user = repository.findByEmail(email);
        if (user != null && passwordMatches(password, user.getPassword())) {
            Employee employee = employeeRepository.findByUserId(user.getId());
            return convertEmployeeEntityToDto(employee, user);
        } else {
            return null;
        }
    }

    private boolean passwordMatches(String rawPassword, String encodedPassword) {
        return rawPassword.equals(encodedPassword);
    }

    private EmployeeDto convertEmployeeEntityToDto(Employee employee, User user) {
        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setId(employee.getId());
        employeeDto.setName(employee.getName());
        employeeDto.setLast_name(employee.getLast_name());
        employeeDto.setBirthdate(employee.getBirthdate());
        employeeDto.setGender(employee.getGender());
        employeeDto.setCod_area(employee.getCod_area());
        employeeDto.setNumber_phone(employee.getNumber_phone());
        employeeDto.setIdentityCard(employee.getIdentity_card());
        employeeDto.setExtensionIdentityCard(employee.getExtension_identity_card());
        employeeDto.setAddress(employee.getAddress());
        employeeDto.setPhoto(employee.getPhoto());
        employeeDto.setShift(employee.getShiftDto());
        employeeDto.setEmail(user.getEmail());
        employeeDto.setTypeUser(user.getTypeUserDto());
        return employeeDto;
    }

    /*private UserDto convertEntityToDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setEmail(user.getEmail());
        userDto.setPassword(user.getPassword());

        return userDto;
    }*/

    private UserDto convertEntityToDto(User user){
        modelMapper.getConfiguration()
                .setMatchingStrategy(MatchingStrategies.LOOSE);
        UserDto userDto = new UserDto();
        userDto = modelMapper.map(user, UserDto.class);
        return userDto;
    }

    private User convertDtoToEntity(UserDto userDto){
        modelMapper.getConfiguration()
                .setMatchingStrategy(MatchingStrategies.LOOSE);
        User user = new User();
        user = modelMapper.map(userDto, User.class);
        return user;
    }

    private TypeUserDto convertEntityToDtoB(User user){
        modelMapper.getConfiguration()
                .setMatchingStrategy(MatchingStrategies.LOOSE);
        TypeUserDto userDto = new TypeUserDto();
        userDto = modelMapper.map(user, TypeUserDto.class);
        return userDto;
    }
}
