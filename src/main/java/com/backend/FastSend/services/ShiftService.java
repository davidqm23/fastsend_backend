package com.backend.FastSend.services;

import com.backend.FastSend.models.dto.ShiftDto;

import java.util.List;

public interface ShiftService {
    List<ShiftDto> findAll();

}
