package com.backend.FastSend.services;

import com.backend.FastSend.models.dto.ShipmenRateDto;
import com.backend.FastSend.models.entities.*;
import com.backend.FastSend.models.request.RegisterShipmentRateRequest;
import com.backend.FastSend.repositories.BranchRepository;
import com.backend.FastSend.repositories.ShipmentRateRepository;
import com.backend.FastSend.repositories.ShipmentTypeRepository;
import com.backend.FastSend.repositories.ShippingTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ShipmentRateServiceImpl implements ShipmentRateService {
    @Autowired
    private ShipmentRateRepository shipmentRateRepository;

    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private ShipmentTypeRepository shipmentTypeRepository;

    @Autowired
    private ShippingTypeRepository shippingTypeRepository;

    @Override
    @Transactional(readOnly = true)
    public List<ShipmenRateDto> findAll() {
        return shipmentRateRepository.findAll()
                .stream()
                .map(this::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public ShipmenRateDto save(RegisterShipmentRateRequest request) {
        ShipmentRate shipmentRate = convertDtoToEntity(request);
        ShipmentRate saveShipmentRate1 = shipmentRateRepository.save(shipmentRate); // Guardar el Company
        return convertEntityToDto(saveShipmentRate1);
    }

    private ShipmenRateDto convertEntityToDto(ShipmentRate shipmentRate) {
        ShipmenRateDto shipmenRateDto = new ShipmenRateDto();
        shipmenRateDto.setId(shipmentRate.getId());
        shipmenRateDto.setPrice(shipmentRate.getPrice());
        shipmenRateDto.setVisible(shipmentRate.getVisible());
        shipmenRateDto.setRemoved(shipmentRate.getRemoved());
        shipmenRateDto.setShippingType(shipmentRate.getShippingTypeDto());
        shipmenRateDto.setShipmentType(shipmentRate.getShipmentTypeDto());
        shipmenRateDto.setBranchOrigin(shipmentRate.getBranchOriginDto());
        shipmenRateDto.setBranchDestination(shipmentRate.getBranchDestinationDto());
        return shipmenRateDto;
    }

    private ShipmentRate convertDtoToEntity(RegisterShipmentRateRequest request) {
        ShipmentRate shipmentRate = new ShipmentRate();
        if (request.getIdBranchOrigen() != null) {
            Branch branch = branchRepository.findById(request.getIdBranchOrigen()).orElse(null);
            shipmentRate.setBranchOrigin(branch);
        }
        if (request.getIdBranchDestination() != null) {
            Branch branch = branchRepository.findById(request.getIdBranchDestination()).orElse(null);
            shipmentRate.setBranchDestination(branch);
        }
        if (request.getIdTypeShipping() != null) {
            ShippingType shippingType = shippingTypeRepository.findById(request.getIdTypeShipping()).orElse(null);
            shipmentRate.setShippingType(shippingType);
        }
        if (request.getIdTypeShipment() != null) {
            ShipmentType shipmentType = shipmentTypeRepository.findById(request.getIdTypeShipment()).orElse(null);
            shipmentRate.setShipmentType(shipmentType);
        }
        shipmentRate.setPrice(request.getPrice());
        shipmentRate.setRemoved(false);
        shipmentRate.setVisible(true);
        return shipmentRate;
    }
}