package com.backend.FastSend.services;

import com.backend.FastSend.models.dto.ShipmentDto;
import com.backend.FastSend.models.dto.ShipmentTypeDto;
import com.backend.FastSend.models.dto.ShippingTypeDto;
import com.backend.FastSend.models.request.RegisterShipmentRequest;

import java.util.List;
import java.util.Optional;

public interface ShipmentService {
    List<ShipmentDto> findAll();
    List<ShipmentTypeDto> findAllShipmentType();
    Optional<ShipmentDto> findById(Long id);
    List<ShipmentDto> findByIdEmployee(Long id);
    Optional<ShipmentDto> save(RegisterShipmentRequest shipmentRequest);
    ShipmentDto saveDelivery(Long id, Long idEmployee);
    ShipmentDto savePaid(Long id);
    Double calculateShipmentCost(Integer quantity, Long idBranchDestination,
                                 Long idBranchOrigin, Long idShipmentType, Long idShippingType);
}
