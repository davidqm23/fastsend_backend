package com.backend.FastSend.services;

import com.backend.FastSend.models.dto.BranchDto;
import com.backend.FastSend.models.dto.ShipmentDto;
import com.backend.FastSend.models.entities.Branch;
import com.backend.FastSend.repositories.BranchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BranchServiceImpl implements BranchService{
    @Autowired
    private BranchRepository branchRepository;

    @Override
    @Transactional(readOnly = true)
    public List<BranchDto> findAll() {
        return branchRepository.findAll()
                .stream()
                .map(this::convertEntityToDto)
                .collect(Collectors.toList());
    }

    private BranchDto convertEntityToDto(Branch branch) {
        BranchDto branchDto = new BranchDto();
        branchDto.setId(branch.getId());
        branchDto.setName(branch.getName());
        branchDto.setAddress(branch.getAddress());
        branchDto.setVisible(branch.getVisible());
        branchDto.setRemoved(branch.getRemoved());
        branchDto.setCity(branch.getCityDto());
        return branchDto;
    }
}
