package com.backend.FastSend.services;

import com.backend.FastSend.models.dto.EmployeeDto;
import com.backend.FastSend.models.request.RegisterEmployeeRequest;

import java.util.List;
import java.util.Optional;

public interface EmployeeService {
    List<EmployeeDto> findAll();

    Optional<EmployeeDto> findById(Long id);

    EmployeeDto saveAdministrator(RegisterEmployeeRequest employeeRequest);

    EmployeeDto saveReceptionist(RegisterEmployeeRequest employeeRequest);
}
