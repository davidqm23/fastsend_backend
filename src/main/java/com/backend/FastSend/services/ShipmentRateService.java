package com.backend.FastSend.services;

import com.backend.FastSend.models.dto.EmployeeDto;
import com.backend.FastSend.models.dto.ShipmenRateDto;
import com.backend.FastSend.models.request.RegisterEmployeeRequest;
import com.backend.FastSend.models.request.RegisterShipmentRateRequest;

import java.util.List;

public interface ShipmentRateService {
    List<ShipmenRateDto> findAll();

    ShipmenRateDto save(RegisterShipmentRateRequest request);

}
