package com.backend.FastSend.services;

import com.backend.FastSend.models.dto.BranchDto;
import com.backend.FastSend.models.dto.CustomerDto;
import com.backend.FastSend.models.entities.Branch;
import com.backend.FastSend.models.entities.Customer;
import com.backend.FastSend.models.entities.Employee;
import com.backend.FastSend.models.request.RegisterCustomerRequest;
import com.backend.FastSend.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerServiceImpl implements CustomerService{
    @Autowired
    private CustomerRepository customerRepository;

    @Override
    @Transactional(readOnly = true)
    public List<CustomerDto> findAll() {
        return customerRepository.findAll()
                .stream()
                .map(this::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<CustomerDto> searchByName(String searchTerm, int maxResults) {
        List<Customer> allCustomers = customerRepository.findAll();

        List<CustomerDto> filteredCustomers = allCustomers.stream()
                .filter(customer -> customer.getName().toLowerCase().contains(searchTerm.toLowerCase()))
                .limit(maxResults)
                .map(this::convertEntityToDto)
                .collect(Collectors.toList());

        return filteredCustomers;
    }

    @Override
    @Transactional
    public CustomerDto save(RegisterCustomerRequest request) {
        Customer customer = convertDtoToEntity(request);
        Customer savedCustomer = customerRepository.save(customer);
        return convertEntityToDto(savedCustomer);
    }

    private CustomerDto convertEntityToDto(Customer customer) {
        CustomerDto customerDto = new CustomerDto();
        customerDto.setId(customer.getId());
        customerDto.setName(customer.getName());
        customerDto.setLast_name(customer.getLast_name());
        customerDto.setEmail(customer.getEmail());
        customerDto.setBirthdate(customer.getBirthdate());
        customerDto.setGender(customer.getGender());
        customerDto.setCod_area(customer.getCod_area());
        customerDto.setNumber_phone(customer.getNumber_phone());
        customerDto.setIdentityCard(customer.getIdentity_card());
        customerDto.setExtensionIdentityCard(customer.getExtension_identity_card());
        customerDto.setAddress(customer.getAddress());
        customerDto.setRemoved(customer.getRemoved());
        customerDto.setVisible(customer.getVisible());
        return customerDto;
    }

    private Customer convertDtoToEntity(RegisterCustomerRequest request) {
        Customer customer = new Customer();
        customer.setName(request.getName());
        customer.setLast_name(request.getLastName());
        customer.setEmail(request.getEmail());
        customer.setBirthdate(request.getBirthdate());
        customer.setGender(request.getGender());
        customer.setCod_area(request.getCod_area());
        customer.setNumber_phone(request.getNumberPhone());
        customer.setIdentity_card(request.getIdentityCard());
        customer.setExtension_identity_card(request.getExtensionIdentityCard());
        customer.setAddress(request.getAddress());
        customer.setRemoved(false);
        customer.setVisible(true);

        return customer;
    }
}
