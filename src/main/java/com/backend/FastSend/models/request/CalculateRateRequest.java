package com.backend.FastSend.models.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CalculateRateRequest {
    private Integer quantity;
    private Long idBranchDestination;
    private Long idBranchOrigin;
    private Long idShipmentType;
    private Long idShippingType;
}
