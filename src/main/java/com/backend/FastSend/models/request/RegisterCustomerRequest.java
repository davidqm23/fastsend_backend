package com.backend.FastSend.models.request;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class RegisterCustomerRequest {
    private String name;
    private String lastName;
    private String email;
    private Date birthdate;
    private String gender;
    private String cod_area;
    private String numberPhone;
    private String identityCard;
    private String extensionIdentityCard;
    private String address;
}
