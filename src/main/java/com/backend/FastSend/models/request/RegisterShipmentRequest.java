package com.backend.FastSend.models.request;

import com.backend.FastSend.models.entities.ShipmentDetail;
import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class RegisterShipmentRequest {
    private String observation;
    private Double total_shipping_rate;
    private Boolean paid;
    private Long idShippingType;
    private Long idCustomerSender;
    private Long idCustomerAddressee;
    private Long idEmployee;
    private Long idBranchOrigin;
    private Long idBranchDestination;
    private List<ShipmentDetailRequest> shipmentDetails;

}
