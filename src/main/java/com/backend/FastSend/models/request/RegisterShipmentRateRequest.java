package com.backend.FastSend.models.request;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SecondaryRow;

import java.util.Date;

@Getter
@Setter
public class RegisterShipmentRateRequest {
    private Long idBranchOrigen;
    private Long idBranchDestination;
    private Long idTypeShipping;
    private Long idTypeShipment;
    private Double price;
}
