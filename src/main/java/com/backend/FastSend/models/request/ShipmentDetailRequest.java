package com.backend.FastSend.models.request;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ShipmentDetailRequest {
    private String content;
    private Double weight;
    private Double additional_volume_surcharge;
    private Double shipping_cost;
    private Boolean removed;
    private Long idShipmentType;
    private Long idShipment;

}
