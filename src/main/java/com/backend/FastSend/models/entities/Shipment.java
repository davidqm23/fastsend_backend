package com.backend.FastSend.models.entities;

import com.backend.FastSend.models.dto.*;
import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Entity
public class Shipment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDateTime date_time_reception;
    private LocalDateTime date_time_delivery;
    @Column(nullable = true, columnDefinition = "TEXT")
    private String observation;
    @Column(nullable = false)
    private Double total_shipping_rate;
    @Column(nullable = false)
    private Boolean paid;
    @Column(nullable = false)
    private Long state;
    @Column(nullable = false)
    private Boolean visible;
    @Column(nullable = false)
    private Boolean removed;
    @ManyToOne
    @JoinColumn(name = "id_shipping_type")
    private ShippingType shippingType;
    @ManyToOne
    @JoinColumn(name = "id_customer_sender")
    private Customer customerSender;
    @ManyToOne
    @JoinColumn(name = "id_customer_addressee")
    private Customer customerAddressee;
    @ManyToOne
    @JoinColumn(name = "id_employee_receptionist")
    private Employee employeeReceptionist;
    @ManyToOne
    @JoinColumn(nullable = true, name = "id_employee_delivery")
    private Employee employeeDelivery;
    @ManyToOne
    @JoinColumn(name = "id_branch_origin")
    private Branch branchOrigin;
    @ManyToOne
    @JoinColumn(name = "id_branch_destination")
    private Branch branchDestination;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "shipment")
    private List<ShipmentDetail> shipmentDetails = new ArrayList<>();

    public ShippingTypeDto getShippingTypeDto() {
        ShippingTypeDto shippingTypeDto = new ShippingTypeDto();
        if (shippingType != null) {
            shippingTypeDto.setId(shippingType.getId());
            shippingTypeDto.setName(shippingType.getName());
        }
        return shippingTypeDto;
    }

    public CustomerDto getCustomerSenderDto() {
        CustomerDto customerDto = new CustomerDto();
        if (customerSender != null) {
            customerDto.setId(customerSender.getId());
            customerDto.setName(customerSender.getName());
            customerDto.setLast_name(customerSender.getLast_name());
            customerDto.setBirthdate(customerSender.getBirthdate());
            customerDto.setGender(customerSender.getGender());
            customerDto.setCod_area(customerSender.getCod_area());
            customerDto.setNumber_phone(customerSender.getNumber_phone());
            customerDto.setAddress(customerSender.getAddress());
            customerDto.setVisible(customerSender.getVisible());
            customerDto.setRemoved(customerSender.getRemoved());
        }
        return customerDto;
    }

    public CustomerDto getCustomerAdresseeDto() {
        CustomerDto customerDto = new CustomerDto();
        if (customerAddressee != null) {
            customerDto.setId(customerAddressee.getId());
            customerDto.setName(customerAddressee.getName());
            customerDto.setLast_name(customerAddressee.getLast_name());
            customerDto.setBirthdate(customerAddressee.getBirthdate());
            customerDto.setGender(customerAddressee.getGender());
            customerDto.setCod_area(customerAddressee.getCod_area());
            customerDto.setNumber_phone(customerAddressee.getNumber_phone());
            customerDto.setAddress(customerAddressee.getAddress());
            customerDto.setVisible(customerAddressee.getVisible());
            customerDto.setRemoved(customerAddressee.getRemoved());
        }
        return customerDto;
    }
    public EmployeeDto getEmployeeReceptionistDto() {
        EmployeeDto employeeDto = new EmployeeDto();
        if (employeeReceptionist != null) {
            employeeDto.setId(employeeReceptionist.getId());
            employeeDto.setName(employeeReceptionist.getName());
            employeeDto.setLast_name(employeeReceptionist.getLast_name());
            employeeDto.setEmail(employeeReceptionist.getLast_name());
            employeeDto.setBirthdate(employeeReceptionist.getBirthdate());
            employeeDto.setGender(employeeReceptionist.getGender());
            employeeDto.setCod_area(employeeReceptionist.getCod_area());
            employeeDto.setNumber_phone(employeeReceptionist.getNumber_phone());
            employeeDto.setAddress(employeeReceptionist.getAddress());
            employeeDto.setPhoto(employeeReceptionist.getPhoto());
            employeeDto.setShift(employeeReceptionist.getShiftDto());
        }
        return employeeDto;
    }
    public EmployeeDto getEmployeeDeliveryDto() {
        EmployeeDto employeeDto = new EmployeeDto();
        if (employeeDelivery != null) {
            employeeDto.setId(employeeDelivery.getId());
            employeeDto.setName(employeeDelivery.getName());
            employeeDto.setLast_name(employeeDelivery.getLast_name());
            employeeDto.setEmail(employeeDelivery.getLast_name());
            employeeDto.setBirthdate(employeeDelivery.getBirthdate());
            employeeDto.setGender(employeeDelivery.getGender());
            employeeDto.setCod_area(employeeDelivery.getCod_area());
            employeeDto.setNumber_phone(employeeDelivery.getNumber_phone());
            employeeDto.setAddress(employeeDelivery.getAddress());
            employeeDto.setPhoto(employeeDelivery.getPhoto());
            employeeDto.setShift(employeeDelivery.getShiftDto());
        }

        return employeeDto;
    }

    public BranchDto getBranchOriginDto() {
        BranchDto branchDto = new BranchDto();
        if (branchOrigin != null) {
            branchDto.setId(branchOrigin.getId());
            branchDto.setName(branchOrigin.getName());
            branchDto.setAddress(branchOrigin.getAddress());
            branchDto.setVisible(branchOrigin.getVisible());
            branchDto.setRemoved(branchOrigin.getRemoved());
            branchDto.setCity(branchOrigin.getCityDto());
        }
        return branchDto;
    }

    public BranchDto getBranchDestinationDto() {
        BranchDto branchDto = new BranchDto();
        if (branchDestination != null) {
            branchDto.setId(branchDestination.getId());
            branchDto.setName(branchDestination.getName());
            branchDto.setAddress(branchDestination.getAddress());
            branchDto.setVisible(branchDestination.getVisible());
            branchDto.setRemoved(branchDestination.getRemoved());
            branchDto.setCity(branchDestination.getCityDto());
        }
        return branchDto;
    }

    public List<ShipmentDetailDto> getShipmentDetailDto() {
        List<ShipmentDetailDto> shipmentDetailDtoList = new ArrayList<>();
            for (ShipmentDetail shipmentDetailItem : shipmentDetails) {
                ShipmentDetailDto shipmentDetailDto = new ShipmentDetailDto();
                shipmentDetailDto.setId(shipmentDetailItem.getId());
                shipmentDetailDto.setContent(shipmentDetailItem.getContent());
                shipmentDetailDto.setWeight(shipmentDetailItem.getWeight());
                shipmentDetailDto.setAdditional_volume_surcharge(shipmentDetailItem.getAdditional_volume_surcharge());
                shipmentDetailDto.setShipping_cost(shipmentDetailItem.getShipping_cost());
                shipmentDetailDto.setRemoved(shipmentDetailItem.getRemoved());
                shipmentDetailDto.setShipmentType(shipmentDetailItem.getShipmentTypeDto());
                shipmentDetailDtoList.add(shipmentDetailDto);
            }
        return shipmentDetailDtoList;
    }
}
