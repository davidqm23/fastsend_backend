package com.backend.FastSend.models.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Data
@Entity
public class TypeUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, unique = true)
    private String name;
    @Column(nullable = false)
    private Boolean visible;
    @Column(nullable = false)
    private Boolean removed;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "typeUser")
    private List<User> user;
}
