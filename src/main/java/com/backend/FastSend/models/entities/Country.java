package com.backend.FastSend.models.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.sql.Time;
import java.util.List;

@Data
@Entity
public class Country {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 30)
    private String name;
    @Column(nullable = false, length = 10)
    private String abbreviation;
    @Column(nullable = false)
    private Boolean visible;
    @Column(nullable = false)
    private Boolean removed;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "country")
    private List<City> cities;
}
