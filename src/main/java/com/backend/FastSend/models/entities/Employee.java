package com.backend.FastSend.models.entities;

import com.backend.FastSend.models.dto.ShiftDto;
import com.backend.FastSend.models.dto.UserDto;
import com.backend.FastSend.models.entities.User;
import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@Entity
public class Employee{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 50)
    private String name;
    @Column(nullable = false, length = 80)
    private String last_name;
    @Column(nullable = false)
    private Date birthdate;
    @Column(nullable = false, length = 20)
    private String gender;
    @Column(nullable = false, length = 20)
    private String cod_area;
    @Column(nullable = false, length = 15)
    private String number_phone;
    @Column(nullable = false, length = 20)
    private String identity_card;
    @Column(nullable = false, length = 2)
    private String extension_identity_card;
    @Column(nullable = false)
    private String address;
    @Column(columnDefinition = "TEXT")
    private String photo;
    @Column(nullable = false)
    private Boolean visible;
    @Column(nullable = false)
    private Boolean removed;
    @OneToOne
    @JoinColumn(name = "id_user")
    private User user;
    @ManyToOne
    @JoinColumn(name = "id_shift")
    private Shift shift;


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employeeReceptionist")
    private List<Shipment> shipmentsEmployeeReceptionist;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employeeDelivery")
    private List<Shipment> shipmentsEmployeeDelivery;

    public ShiftDto getShiftDto() {
        ShiftDto shiftDto = new ShiftDto();
        if (shiftDto != null) {
            shiftDto.setId(shift.getId());
            shiftDto.setName(shift.getName());
            shiftDto.setStart_time(shift.getStart_time());
            shiftDto.setEnd_time(shift.getEnd_time());
        }
        return shiftDto;
    }

    public UserDto getUserDto() {
        UserDto userDto = new UserDto();
        if (userDto != null) {
            userDto.setId(user.getId());
            userDto.setEmail(user.getEmail());
            userDto.setTypeUser(user.getTypeUserDto());
        }
        return userDto;
    }
}
