package com.backend.FastSend.models.entities;

import com.backend.FastSend.models.dto.CityDto;
import com.backend.FastSend.models.dto.ShippingTypeDto;
import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@Entity
public class Branch {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 50)
    private String name;
    @Column(nullable = false, length = 200)
    private String address;
    @Column(nullable = false)
    private Boolean visible;
    @Column(nullable = false)
    private Boolean removed;
    @ManyToOne
    @JoinColumn(name = "id_city")
    private City city;
    @ManyToOne
    @JoinColumn(name = "id_company")
    private Company company;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "branchOrigin")
    private List<Shipment> shipmentsBranchOrigin;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "branchDestination")
    private List<Shipment> shipmentsBranchDestination;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "branchOrigin")
    private List<ShipmentRate> shipmentRatesBranchOrigin;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "branchDestination")
    private List<ShipmentRate> shipmentRatesBranchDestination;

    public CityDto getCityDto() {
        CityDto cityDto = new CityDto();
        if (cityDto != null) {
            cityDto.setId(city.getId());
            cityDto.setName(city.getName());
            cityDto.setAbbreviation(city.getAbbreviation());
            cityDto.setVisible(city.getVisible());
            cityDto.setRemoved(city.getRemoved());
        }
        return cityDto;
    }
}
