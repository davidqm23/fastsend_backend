package com.backend.FastSend.models.entities;

import com.backend.FastSend.models.dto.ShiftDto;
import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@Entity
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 50)
    private String name;
    @Column(nullable = false, length = 80)
    private String last_name;
    @Column(nullable = false, length = 200)
    private String email;
    @Column(nullable = false)
    private Date birthdate;
    @Column(nullable = false, length = 20)
    private String gender;
    @Column(nullable = false, length = 20)
    private String cod_area;
    @Column(nullable = false, length = 15)
    private String number_phone;
    @Column(nullable = false, length = 20)
    private String identity_card;
    @Column(nullable = false, length = 2)
    private String extension_identity_card;
    @Column(nullable = false)
    private String address;
    @Column(nullable = false)
    private Boolean visible;
    @Column(nullable = false)
    private Boolean removed;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "customerSender")
    private List<Shipment> shipmentsCustomerSender;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "customerAddressee")
    private List<Shipment> shipmentsCustomerAddressee;

}
