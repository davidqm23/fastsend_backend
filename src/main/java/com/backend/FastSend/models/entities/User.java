package com.backend.FastSend.models.entities;

import com.backend.FastSend.models.dto.TypeUserDto;
import com.backend.FastSend.models.dto.UserDto;
import com.backend.FastSend.models.entities.TypeUser;
import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, unique = true)
    private String email;
    @Column(nullable = false, length = 60)
    private String password;
    private Boolean removed;

    @OneToOne(mappedBy = "user")
    private Employee employee;

    @ManyToOne
    @JoinColumn(name = "id_type_user")
    private TypeUser typeUser;

    public TypeUserDto getTypeUserDto() {
        TypeUserDto typeUserDto = new TypeUserDto();
        if (typeUserDto != null) {
            typeUserDto.setId(typeUser.getId());
            typeUserDto.setName(typeUser.getName());
        }
        return typeUserDto;
    }
}
