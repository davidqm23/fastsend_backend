package com.backend.FastSend.models.entities;

import com.backend.FastSend.models.dto.BranchDto;
import com.backend.FastSend.models.dto.ShipmentDetailDto;
import com.backend.FastSend.models.dto.ShipmentTypeDto;
import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class ShipmentDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, columnDefinition = "TEXT")
    private String content;
    @Column(nullable = false)
    private Double weight;
    @Column(nullable = false)
    private Double additional_volume_surcharge;
    @Column(nullable = false)
    private Double shipping_cost;
    @Column(nullable = false)
    private Boolean removed;
    @ManyToOne
    @JoinColumn(name = "id_shipment_type")
    private ShipmentType shipmentType;
    @ManyToOne
    @JoinColumn(name = "id_shipment")
    private Shipment shipment;

    public ShipmentTypeDto getShipmentTypeDto() {
        ShipmentTypeDto shipmentTypeDto = new ShipmentTypeDto();
        if (shipmentType != null) {
            shipmentTypeDto.setId(shipmentType.getId());
            shipmentTypeDto.setName(shipmentType.getName());
        }
        return shipmentTypeDto;
    }
}
