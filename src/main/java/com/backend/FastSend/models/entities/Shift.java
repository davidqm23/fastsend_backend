package com.backend.FastSend.models.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.sql.Time;
import java.util.Date;
import java.util.List;

@Data
@Entity
public class Shift {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 100)
    private String name;
    @Column(nullable = false)
    private Time start_time;
    @Column(nullable = false)
    private Time end_time;
    @Column(nullable = false)
    private Boolean visible;
    @Column(nullable = false)
    private Boolean removed;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "shift")
    private List<Employee> employees;
}
