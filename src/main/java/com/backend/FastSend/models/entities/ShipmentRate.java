package com.backend.FastSend.models.entities;

import com.backend.FastSend.models.dto.BranchDto;
import com.backend.FastSend.models.dto.ShipmentTypeDto;
import com.backend.FastSend.models.dto.ShippingTypeDto;
import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Entity
public class ShipmentRate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private Double price;
    @Column(nullable = false)
    private Boolean visible;
    @Column(nullable = false)
    private Boolean removed;
    @ManyToOne
    @JoinColumn(name = "id_shipping_type")
    private ShippingType shippingType;
    @ManyToOne
    @JoinColumn(name = "id_shipment_type")
    private ShipmentType shipmentType;
    @ManyToOne
    @JoinColumn(name = "id_branch_origin")
    private Branch branchOrigin;
    @ManyToOne
    @JoinColumn(name = "id_branch_destination")
    private Branch branchDestination;

    public ShippingTypeDto getShippingTypeDto() {
        ShippingTypeDto shippingTypeDto = new ShippingTypeDto();
        if (shippingType != null) {
            shippingTypeDto.setId(shippingType.getId());
            shippingTypeDto.setName(shippingType.getName());
        }
        return shippingTypeDto;
    }

    public ShipmentTypeDto getShipmentTypeDto() {
        ShipmentTypeDto shipmentTypeDto = new ShipmentTypeDto();
        if (shippingType != null) {
            shipmentTypeDto.setId(shipmentType.getId());
            shipmentTypeDto.setName(shipmentType.getName());
        }
        return shipmentTypeDto;
    }

    public BranchDto getBranchOriginDto() {
        BranchDto branchDto = new BranchDto();
        if (branchOrigin != null) {
            branchDto.setId(branchOrigin.getId());
            branchDto.setName(branchOrigin.getName());
            branchDto.setAddress(branchOrigin.getAddress());
            branchDto.setVisible(branchOrigin.getVisible());
            branchDto.setRemoved(branchOrigin.getRemoved());
            branchDto.setCity(branchOrigin.getCityDto());
        }
        return branchDto;
    }

    public BranchDto getBranchDestinationDto() {
        BranchDto branchDto = new BranchDto();
        if (branchDestination != null) {
            branchDto.setId(branchDestination.getId());
            branchDto.setName(branchDestination.getName());
            branchDto.setAddress(branchDestination.getAddress());
            branchDto.setVisible(branchDestination.getVisible());
            branchDto.setRemoved(branchDestination.getRemoved());
            branchDto.setCity(branchDestination.getCityDto());
        }
        return branchDto;
    }
}
