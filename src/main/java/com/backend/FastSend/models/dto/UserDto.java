package com.backend.FastSend.models.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto {
    private Long id;
    private String email;
    private TypeUserDto typeUser;
}
