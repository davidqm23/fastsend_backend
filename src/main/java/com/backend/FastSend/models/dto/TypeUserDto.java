package com.backend.FastSend.models.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TypeUserDto {
    private Long id;
    private String name;
}
