package com.backend.FastSend.models.dto;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class CustomerDto {
    private Long id;
    private String name;
    private String last_name;
    private String email;
    private Date birthdate;
    private String gender;
    private String cod_area;
    private String number_phone;
    private String identityCard;
    private String extensionIdentityCard;
    private String address;
    private Boolean visible;
    private Boolean removed;
}
