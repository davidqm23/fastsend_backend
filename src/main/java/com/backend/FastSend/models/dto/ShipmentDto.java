package com.backend.FastSend.models.dto;

import com.backend.FastSend.models.entities.*;
import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
public class ShipmentDto {
    private Long id;
    private LocalDateTime date_time_reception;
    private LocalDateTime date_time_delivery;
    private String observation;
    private Double total_shipping_rate;
    private Boolean paid;
    private Long state;
    private Boolean visible;
    private Boolean removed;
    private ShippingTypeDto shippingType;
    private CustomerDto customerSender;
    private CustomerDto customerAddressee;
    private EmployeeDto employeeReceptionist;
    private EmployeeDto employeeDelivery;
    private BranchDto branchOrigin;
    private BranchDto branchDestination;
    private List<ShipmentDetailDto> shipmentDetails;
}