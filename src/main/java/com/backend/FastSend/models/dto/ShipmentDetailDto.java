package com.backend.FastSend.models.dto;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShipmentDetailDto {
    private Long id;
    private String content;
    private Double weight;
    private Double additional_volume_surcharge;
    private Double shipping_cost;
    private ShipmentTypeDto shipmentType;
    private Boolean removed;
}
