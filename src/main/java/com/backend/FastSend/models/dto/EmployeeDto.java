package com.backend.FastSend.models.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class EmployeeDto {
    private Long id;
    private String name;
    private String last_name;
    private String email;
    private Date birthdate;
    private String gender;
    private String cod_area;
    private String number_phone;
    private String identityCard;
    private String extensionIdentityCard;
    private String address;
    private String photo;
    private ShiftDto shift;
    private TypeUserDto typeUser;
}
