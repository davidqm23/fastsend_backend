package com.backend.FastSend.models.dto;

import com.backend.FastSend.models.entities.Branch;
import com.backend.FastSend.models.entities.ShipmentType;
import com.backend.FastSend.models.entities.ShippingType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShipmenRateDto {
    private Long id;
    private Double price;
    private Boolean visible;
    private Boolean removed;
    private ShippingTypeDto shippingType;
    private ShipmentTypeDto shipmentType;
    private BranchDto branchOrigin;
    private BranchDto branchDestination;
}
