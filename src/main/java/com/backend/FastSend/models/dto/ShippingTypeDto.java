package com.backend.FastSend.models.dto;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShippingTypeDto {
    private Long id;
    private String name;
}
