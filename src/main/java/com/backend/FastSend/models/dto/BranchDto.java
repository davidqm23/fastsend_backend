package com.backend.FastSend.models.dto;

import com.backend.FastSend.models.entities.City;
import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BranchDto {
    private Long id;
    private String name;
    private String address;
    private Boolean visible;
    private Boolean removed;
    private CityDto city;

}
