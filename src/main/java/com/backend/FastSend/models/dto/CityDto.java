package com.backend.FastSend.models.dto;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class CityDto {
    private Long id;
    private String name;
    private String abbreviation;
    private Boolean visible;
    private Boolean removed;
}

