package com.backend.FastSend.models.dto;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

import java.sql.Time;

@Getter
@Setter
public class ShiftDto {
    private Long id;
    private String name;
    private Time start_time;
    private Time end_time;
}
