package com.backend.FastSend.models.base;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import java.time.LocalDateTime;

@JsonPropertyOrder({ "code", "dateTime", "message", "data" })
public class ResponseData<T> {
    private final int Code;

    private final String Message;

    private final LocalDateTime DateTime;

    private final T Data;

    public ResponseData(int code, LocalDateTime dateTime, String message, T data) {
        this.Code = code;
        this.Message = message;
        this.DateTime = dateTime;
        this.Data = data;
    }

    public int getCode() {
        return Code;
    }

    public String getMessage() {
        return Message;
    }

    public LocalDateTime getDateTime() {
        return DateTime;
    }

    public T getData() {
        return Data;
    }
}
