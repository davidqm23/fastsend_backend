package com.backend.FastSend.repositories;

import com.backend.FastSend.models.entities.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    @Query("SELECT p FROM Employee p WHERE p.user.id = :userId")
    Employee findByUserId(@Param("userId") Long userId);
}
