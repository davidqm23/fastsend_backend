package com.backend.FastSend.repositories;

import com.backend.FastSend.models.entities.Shipment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShipmentRepository extends JpaRepository<Shipment, Long> {
}
