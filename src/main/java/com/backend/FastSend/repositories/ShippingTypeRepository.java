package com.backend.FastSend.repositories;

import com.backend.FastSend.models.entities.ShippingType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShippingTypeRepository extends JpaRepository<ShippingType, Long> {
}
