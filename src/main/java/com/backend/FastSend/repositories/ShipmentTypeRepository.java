package com.backend.FastSend.repositories;

import com.backend.FastSend.models.entities.ShipmentType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShipmentTypeRepository extends JpaRepository<ShipmentType, Long> {
}
