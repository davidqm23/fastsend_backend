package com.backend.FastSend.repositories;

import com.backend.FastSend.models.entities.ShipmentDetail;
import com.backend.FastSend.models.entities.ShipmentRate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ShipmentRateRepository extends JpaRepository<ShipmentRate, Long> {
    @Query("SELECT sr FROM ShipmentRate sr WHERE sr.branchDestination.id = :idBranchDestination " +
            "AND sr.branchOrigin.id = :idBranchOrigin " +
            "AND sr.shipmentType.id = :idShipmentType " +
            "AND sr.shippingType.id = :idShippingType")
    ShipmentRate findByCriteria(
            @Param("idBranchDestination") Long idBranchDestination,
            @Param("idBranchOrigin") Long idBranchOrigin,
            @Param("idShipmentType") Long idShipmentType,
            @Param("idShippingType") Long idShippingType
    );
}
