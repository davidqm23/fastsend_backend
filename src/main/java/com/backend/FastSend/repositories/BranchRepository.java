package com.backend.FastSend.repositories;

import com.backend.FastSend.models.entities.Branch;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BranchRepository extends JpaRepository<Branch, Long> {
}
