package com.backend.FastSend.repositories;

import com.backend.FastSend.models.entities.Shift;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShiftRepository extends JpaRepository<Shift, Long> {
}
