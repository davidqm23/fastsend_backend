package com.backend.FastSend.repositories;

import com.backend.FastSend.models.entities.Employee;
import com.backend.FastSend.models.entities.TypeUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TypeUserRepository extends JpaRepository<TypeUser, Long> {
}
