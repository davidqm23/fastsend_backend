package com.backend.FastSend.repositories;

import com.backend.FastSend.models.entities.ShipmentDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShipmentDetailRepository extends JpaRepository<ShipmentDetail, Long> {
}
