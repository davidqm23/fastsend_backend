package com.backend.FastSend.controller;

import com.backend.FastSend.models.base.NotFoundException;
import com.backend.FastSend.models.base.ResponseData;
import com.backend.FastSend.models.dto.EmployeeDto;
import com.backend.FastSend.models.dto.ShipmentDto;
import com.backend.FastSend.models.dto.ShipmentTypeDto;
import com.backend.FastSend.models.dto.ShippingTypeDto;
import com.backend.FastSend.models.entities.ShipmentType;
import com.backend.FastSend.models.request.CalculateRateRequest;
import com.backend.FastSend.models.request.RegisterEmployeeRequest;
import com.backend.FastSend.models.request.RegisterShipmentRequest;
import com.backend.FastSend.services.ShipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/shipment")
@CrossOrigin(origins = {"http://localhost:4200"})
public class ShipmentController {

    @Autowired
    private ShipmentService service;

    @GetMapping
    public ResponseEntity<ResponseData<List<ShipmentDto>>> list() {
        try{
            List<ShipmentDto> shipmentDtos = service.findAll();
            ResponseData<List<ShipmentDto>> responseData = new ResponseData<>(0, LocalDateTime.now(),"Success", shipmentDtos);
            return ResponseEntity.ok(responseData);
        } catch (Exception e) {
            ResponseData<List<ShipmentDto>> responseData = new ResponseData<>(1, LocalDateTime.now(),  e.getMessage(), null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
        }
    }

    @GetMapping("/ShipmentType")
    public ResponseEntity<ResponseData<List<ShipmentTypeDto>>> listTypeShipment() {
        try{
            List<ShipmentTypeDto> shipmentTypeDtos = service.findAllShipmentType();
            ResponseData<List<ShipmentTypeDto>> responseData = new ResponseData<>(0, LocalDateTime.now(),"Success", shipmentTypeDtos);
            return ResponseEntity.ok(responseData);
        } catch (Exception e) {
            ResponseData<List<ShipmentTypeDto>> responseData = new ResponseData<>(1, LocalDateTime.now(),  e.getMessage(), null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseData<Optional<ShipmentDto>>> show(@PathVariable Long id) {
        try {
            Optional<ShipmentDto> shipmentDto = service.findById(id);
            ResponseData<Optional<ShipmentDto>> responseData = new ResponseData<>(0, LocalDateTime.now(),"Success", shipmentDto);
            return ResponseEntity.ok(responseData);
        } catch (Exception e) {
            ResponseData<Optional<ShipmentDto>> responseData = new ResponseData<>(1, LocalDateTime.now(),  e.getMessage(), null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
        }
    }

    @GetMapping("/employee/{id}")
    public ResponseEntity<ResponseData<List<ShipmentDto>>> listCompany(@PathVariable Long id) {
        try{
            List<ShipmentDto> job = service.findByIdEmployee(id);
            ResponseData<List<ShipmentDto>> responseData = new ResponseData<>(0, LocalDateTime.now(),"Success", job);
            return ResponseEntity.ok(responseData);
        } catch (Exception e) {
            ResponseData<List<ShipmentDto>> responseData = new ResponseData<>(1, LocalDateTime.now(),  e.getMessage(), null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
        }
    }

    @PostMapping
    public ResponseEntity<ResponseData<Optional<ShipmentDto>>> createAdministrator(@RequestBody RegisterShipmentRequest request) {
        try {
            Optional<ShipmentDto> shipmentDto = service.save(request);
            ResponseData<Optional<ShipmentDto>> responseData = new ResponseData<>(0, LocalDateTime.now(), "Success", shipmentDto);
            return ResponseEntity.status(HttpStatus.CREATED).body(responseData);
        } catch (Exception e) {
            ResponseData<Optional<ShipmentDto>> responseData = new ResponseData<>(1, LocalDateTime.now(), e.getMessage(), null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
        }
    }

    @GetMapping("/SaveDelivery/idShipment={idShipment}&idEmployee={idEmployee}")
    public ResponseEntity<ResponseData<ShipmentDto>> saveDelivery(@PathVariable Long idShipment, @PathVariable Long idEmployee) {
        try {
            ShipmentDto shipmentDto = service.saveDelivery(idShipment, idEmployee);
            ResponseData<ShipmentDto> responseData = new ResponseData<>(0, LocalDateTime.now(), "Success", shipmentDto);
            return ResponseEntity.status(HttpStatus.CREATED).body(responseData);
        } catch (RuntimeException e) {
            ResponseData<ShipmentDto> responseData = new ResponseData<>(1, LocalDateTime.now(), e.getMessage(), null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        } catch (Exception e) {
            ResponseData<ShipmentDto> responseData = new ResponseData<>(1, LocalDateTime.now(), e.getMessage(), null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
        }
    }

    @GetMapping("/SavePaid/idShipment={idShipment}")
    public ResponseEntity<ResponseData<ShipmentDto>> savePaid(@PathVariable Long idShipment) {
        try {
            ShipmentDto shipmentDto = service.savePaid(idShipment);
            ResponseData<ShipmentDto> responseData = new ResponseData<>(0, LocalDateTime.now(), "Success", shipmentDto);
            return ResponseEntity.status(HttpStatus.CREATED).body(responseData);
        } catch (Exception e) {
            ResponseData<ShipmentDto> responseData = new ResponseData<>(1, LocalDateTime.now(), e.getMessage(), null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
        }
    }

    @PostMapping("/CalculateRate")
    public ResponseEntity<ResponseData<Double>> calculateRate(@RequestBody CalculateRateRequest request) {
        try {
            Double result = service.calculateShipmentCost(request.getQuantity(), request.getIdBranchDestination(), request.getIdBranchOrigin(), request.getIdShipmentType(), request.getIdShippingType());
            ResponseData<Double> responseData = new ResponseData<>(0, LocalDateTime.now(), "Success", result);

            return ResponseEntity.status(HttpStatus.CREATED).body(responseData);
        } catch (Exception e) {
            ResponseData<Double> responseData = new ResponseData<>(1, LocalDateTime.now(), e.getMessage(), null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
        }
    }
}
