package com.backend.FastSend.controller;

import com.backend.FastSend.models.base.ResponseData;
import com.backend.FastSend.models.dto.EmployeeDto;
import com.backend.FastSend.models.dto.ShipmenRateDto;
import com.backend.FastSend.models.request.RegisterEmployeeRequest;
import com.backend.FastSend.models.request.RegisterShipmentRateRequest;
import com.backend.FastSend.services.EmployeeService;
import com.backend.FastSend.services.ShipmentRateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/shipmentRate")
@CrossOrigin(origins = {"http://localhost:4200"})
public class ShipmentRateController {

    @Autowired
    private ShipmentRateService service;

    @GetMapping
    public ResponseEntity<ResponseData<List<ShipmenRateDto>>> list() {
        try{
            List<ShipmenRateDto> shipmenRateDtos = service.findAll();
            ResponseData<List<ShipmenRateDto>> responseData = new ResponseData<>(0, LocalDateTime.now(),"Success", shipmenRateDtos);
            return ResponseEntity.ok(responseData);
        } catch (Exception e) {
            ResponseData<List<ShipmenRateDto>> responseData = new ResponseData<>(1, LocalDateTime.now(),  e.getMessage(), null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
        }
    }

    @PostMapping
    public ResponseEntity<ResponseData<ShipmenRateDto>> save(@RequestBody RegisterShipmentRateRequest request) {
        try {
            ShipmenRateDto shipmenRateDto = service.save(request);
            ResponseData<ShipmenRateDto> responseData = new ResponseData<>(0, LocalDateTime.now(), "Success", shipmenRateDto);
            return ResponseEntity.status(HttpStatus.CREATED).body(responseData);
        } catch (Exception e) {
            ResponseData<ShipmenRateDto> responseData = new ResponseData<>(1, LocalDateTime.now(), e.getMessage(), null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
        }
    }

}
