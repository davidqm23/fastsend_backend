package com.backend.FastSend.controller;

import com.backend.FastSend.models.base.ResponseData;
import com.backend.FastSend.models.dto.EmployeeDto;
import com.backend.FastSend.models.request.RegisterEmployeeRequest;
import com.backend.FastSend.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/employee")
@CrossOrigin(origins = {"http://localhost:4200"})
public class EmployeeController {
    @Autowired
    private EmployeeService service;

    @GetMapping
    public ResponseEntity<ResponseData<List<EmployeeDto>>> list() {
        try{
            List<EmployeeDto> employee = service.findAll();
            ResponseData<List<EmployeeDto>> responseData = new ResponseData<>(0, LocalDateTime.now(),"Success", employee);
            return ResponseEntity.ok(responseData);
        } catch (Exception e) {
            ResponseData<List<EmployeeDto>> responseData = new ResponseData<>(1, LocalDateTime.now(),  e.getMessage(), null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseData<Optional<EmployeeDto>>> show(@PathVariable Long id) {
        try {
            Optional<EmployeeDto> employeeDto = service.findById(id);
            ResponseData<Optional<EmployeeDto>> responseData = new ResponseData<>(0, LocalDateTime.now(),"Success", employeeDto);
            return ResponseEntity.ok(responseData);
        } catch (Exception e) {
            ResponseData<Optional<EmployeeDto>> responseData = new ResponseData<>(1, LocalDateTime.now(),  e.getMessage(), null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
        }
    }

    @PostMapping("/RegisterAdministrator")
    public ResponseEntity<ResponseData<EmployeeDto>> createAdministrator(@RequestBody RegisterEmployeeRequest request) {
        try {
            EmployeeDto createdUserDto = service.saveAdministrator(request);
            ResponseData<EmployeeDto> responseData = new ResponseData<>(0, LocalDateTime.now(), "Success", createdUserDto);
            return ResponseEntity.status(HttpStatus.CREATED).body(responseData);
        } catch (Exception e) {
            ResponseData<EmployeeDto> responseData = new ResponseData<>(1, LocalDateTime.now(), e.getMessage(), null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
        }
    }

    @PostMapping("/RegisterReceptionist")
    public ResponseEntity<ResponseData<EmployeeDto>> createReceptionist(@RequestBody RegisterEmployeeRequest request) {
        try {
            EmployeeDto createdUserDto = service.saveReceptionist(request);
            ResponseData<EmployeeDto> responseData = new ResponseData<>(0, LocalDateTime.now(), "Success", createdUserDto);
            return ResponseEntity.status(HttpStatus.CREATED).body(responseData);
        } catch (Exception e) {
            ResponseData<EmployeeDto> responseData = new ResponseData<>(1, LocalDateTime.now(), e.getMessage(), null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
        }
    }
}
