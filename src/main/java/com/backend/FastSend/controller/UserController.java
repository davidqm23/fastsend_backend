package com.backend.FastSend.controller;

import com.backend.FastSend.models.base.ResponseData;
import com.backend.FastSend.models.dto.EmployeeDto;
import com.backend.FastSend.models.request.LoginRequest;
import com.backend.FastSend.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/user")
@CrossOrigin(origins = {"http://localhost:4200"})
public class UserController {

    @Autowired
    private UserService service;

    @PostMapping("/login")
    public ResponseEntity<ResponseData<EmployeeDto>> loginRecepcionist(@RequestBody LoginRequest loginRequest) {
        try {
            EmployeeDto employeeDto = service.authenticate(loginRequest.getEmail(), loginRequest.getPassword());
            ResponseData<EmployeeDto> responseData = new ResponseData<>(0, LocalDateTime.now(),"Login successful", employeeDto);
            return ResponseEntity.ok(responseData);
        } catch (Exception e) {
            ResponseData<EmployeeDto> responseData = new ResponseData<>(1, LocalDateTime.now(), e.getMessage(), null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
        }
    }
}
