package com.backend.FastSend.controller;

import com.backend.FastSend.models.base.ResponseData;
import com.backend.FastSend.models.dto.EmployeeDto;
import com.backend.FastSend.models.dto.ShiftDto;
import com.backend.FastSend.services.ShiftService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/shift")
@CrossOrigin(origins = {"http://localhost:4200"})
public class ShiftController {
    @Autowired
    private ShiftService service;

    @GetMapping
    public ResponseEntity<ResponseData<List<ShiftDto>>> list() {
        try{
            List<ShiftDto> shiftDtos = service.findAll();
            ResponseData<List<ShiftDto>> responseData = new ResponseData<>(0, LocalDateTime.now(),"Success", shiftDtos);
            return ResponseEntity.ok(responseData);
        } catch (Exception e) {
            ResponseData<List<ShiftDto>> responseData = new ResponseData<>(1, LocalDateTime.now(),  e.getMessage(), null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
        }
    }
}
