package com.backend.FastSend.controller;

import com.backend.FastSend.models.base.ResponseData;
import com.backend.FastSend.models.dto.BranchDto;
import com.backend.FastSend.models.dto.ShipmentDto;
import com.backend.FastSend.services.BranchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/branch")
@CrossOrigin(origins = {"http://localhost:4200"})
public class BranchController {

    @Autowired
    private BranchService service;

    @GetMapping
    public ResponseEntity<ResponseData<List<BranchDto>>> list() {
        try{
            List<BranchDto> branchDtos = service.findAll();
            ResponseData<List<BranchDto>> responseData = new ResponseData<>(0, LocalDateTime.now(),"Success", branchDtos);
            return ResponseEntity.ok(responseData);
        } catch (Exception e) {
            ResponseData<List<BranchDto>> responseData = new ResponseData<>(1, LocalDateTime.now(),  e.getMessage(), null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
        }
    }
}
