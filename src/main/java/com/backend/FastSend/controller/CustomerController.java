package com.backend.FastSend.controller;

import com.backend.FastSend.models.base.ResponseData;
import com.backend.FastSend.models.dto.CustomerDto;
import com.backend.FastSend.models.request.RegisterCustomerRequest;
import com.backend.FastSend.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/customer")
@CrossOrigin(origins = {"http://localhost:4200"})
public class CustomerController {
    @Autowired
    private CustomerService service;

    @GetMapping("/All")
    public ResponseEntity<ResponseData<List<CustomerDto>>> list() {
        try{
            List<CustomerDto> customerDtos = service.findAll();
            ResponseData<List<CustomerDto>> responseData = new ResponseData<>(0, LocalDateTime.now(),"Success", customerDtos);
            return ResponseEntity.ok(responseData);
        } catch (Exception e) {
            ResponseData<List<CustomerDto>> responseData = new ResponseData<>(1, LocalDateTime.now(),  e.getMessage(), null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
        }
    }

    @GetMapping
    public ResponseEntity<ResponseData<List<CustomerDto>>> list(
            @RequestParam(name = "searchTerm", required = false, defaultValue = "") String searchTerm,
            @RequestParam(name = "maxResults", required = false, defaultValue = "10") int maxResults
    ) {
        try {
            List<CustomerDto> customerDtos;

            if (!searchTerm.isEmpty()) {
                customerDtos = service.searchByName(searchTerm, maxResults);
            } else {
                customerDtos = service.findAll();
            }

            ResponseData<List<CustomerDto>> responseData = new ResponseData<>(0, LocalDateTime.now(), "Success", customerDtos);
            return ResponseEntity.ok(responseData);
        } catch (Exception e) {
            ResponseData<List<CustomerDto>> responseData = new ResponseData<>(1, LocalDateTime.now(), e.getMessage(), null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
        }
    }

    @PostMapping()
    public ResponseEntity<ResponseData<CustomerDto>> save(@RequestBody RegisterCustomerRequest request) {
        try {
            CustomerDto customerDto = service.save(request);
            ResponseData<CustomerDto> responseData = new ResponseData<>(0, LocalDateTime.now(), "Success", customerDto);
            return ResponseEntity.status(HttpStatus.CREATED).body(responseData);
        } catch (Exception e) {
            ResponseData<CustomerDto> responseData = new ResponseData<>(1, LocalDateTime.now(), e.getMessage(), null);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseData);
        }
    }
}
